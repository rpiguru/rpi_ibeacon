# performs a simple device inquiry, and returns a list of ble advertisements
# discovered device

# NOTE: Python's struct.pack() will add padding bytes unless you make the endianness explicit. Little endian
# should be used for BLE. Always start a struct.pack() format string with "<"
import Queue
import atexit
import datetime
import os
import struct
import sys
import threading
import time
import traceback

import bluetooth._bluetooth as bluez
import rfc3339

from base import Base

LE_META_EVENT = 0x3e
LE_PUBLIC_ADDRESS = 0x00
LE_RANDOM_ADDRESS = 0x01
LE_SET_SCAN_PARAMETERS_CP_SIZE = 7
OGF_LE_CTL = 0x08
OCF_LE_SET_SCAN_PARAMETERS = 0x000B
OCF_LE_SET_SCAN_ENABLE = 0x000C
OCF_LE_CREATE_CONN = 0x000D

LE_ROLE_MASTER = 0x00
LE_ROLE_SLAVE = 0x01

# these are actually subevents of LE_META_EVENT
EVT_LE_CONN_COMPLETE = 0x01
EVT_LE_ADVERTISING_REPORT = 0x02
EVT_LE_CONN_UPDATE_COMPLETE = 0x03
EVT_LE_READ_REMOTE_USED_FEATURES_COMPLETE = 0x04

# Advertisement event types
ADV_IND = 0x00
ADV_DIRECT_IND = 0x01
ADV_SCAN_IND = 0x02
ADV_NONCONN_IND = 0x03
ADV_SCAN_RSP = 0x04


class BeaconScanner(Base):

    sock = None
    packet_queue = Queue.Queue()
    data_queue = Queue.Queue()
    b_stop = False
    data_list = []
    mac = ''

    def __init__(self, dev_id=0):
        Base.__init__(self)
        self.mac = read_local_bdaddr()
        atexit.register(self.key_interrupt)
        try:
            self.sock = bluez.hci_open_dev(dev_id)
        except:
            traceback.print_exc()
            self.sock = None

    def enable_scan(self):
        try:
            cmd_pkt = struct.pack("<BB", 0x01, 0x00)
            bluez.hci_send_cmd(self.sock, OGF_LE_CTL, OCF_LE_SET_SCAN_ENABLE, cmd_pkt)
            return True
        except bluez.error as e:
            print "Failed to enable scan: ", e
            return False

    def disable_scan(self):
        try:
            cmd_pkt = struct.pack("<BB", 0x00, 0x00)
            bluez.hci_send_cmd(self.sock, OGF_LE_CTL, OCF_LE_SET_SCAN_ENABLE, cmd_pkt)
            return True
        except bluez.error as e:
            print "Failed to disable scan: ", e
            return False

    def start(self):
        self.b_stop = False
        if not self.enable_scan():
            print 'Failed to enable scanning function, please check BT module of RPi...'
            return False

        threading.Thread(target=self.receive_packet).start()
        threading.Thread(target=self.noise_filter).start()
        return True

    def receive_packet(self):
        """
        Receive BLE packet and parse it.
        :return:
        """
        while True:
            if self.b_stop:
                time.sleep(.1)
            else:
                old_filter = self.sock.getsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, 14)

                # perform a device inquiry on bluetooth device #0
                # The inquiry should last 8 * 1.28 = 10.24 seconds
                # before the inquiry is performed, bluez should flush its cache of
                # previously discovered devices
                flt = bluez.hci_filter_new()
                bluez.hci_filter_all_events(flt)
                bluez.hci_filter_set_ptype(flt, bluez.HCI_EVENT_PKT)
                self.sock.setsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, flt)

                pkt = self.sock.recv(255)
                ptype, event, plen = struct.unpack("BBB", pkt[:3])

                if event in [bluez.EVT_INQUIRY_RESULT_WITH_RSSI, bluez.EVT_NUM_COMP_PKTS, bluez.EVT_DISCONN_COMPLETE]:
                    pass
                elif event == LE_META_EVENT:
                    subevent, = struct.unpack("B", pkt[3])
                    pkt = pkt[4:]
                    if subevent == EVT_LE_CONN_COMPLETE:
                        self.le_handle_connection_complete(pkt)
                    elif subevent == EVT_LE_ADVERTISING_REPORT:
                        # num_reports = struct.unpack("B", pkt[0])[0]
                        # TODO: Check if num_reports is greater than 1
                        # print "Report number: ", num_reports

                        report_pkt_offset = 0

                        mac = packed_bdaddr_to_string(pkt[report_pkt_offset + 3:report_pkt_offset + 9])
                        uuid = to_string(pkt[report_pkt_offset - 22: report_pkt_offset - 6])
                        major = to_number(pkt[report_pkt_offset - 6: report_pkt_offset - 4])
                        minor = to_number(pkt[report_pkt_offset - 4: report_pkt_offset - 2])
                        tx_power = struct.unpack("b", pkt[report_pkt_offset - 2])[0]
                        rssi = struct.unpack("b", pkt[report_pkt_offset - 1])[0]

                        result = dict()
                        result['macid'] = mac
                        result['uuid'] = uuid
                        result['major'] = major
                        result['minor'] = minor
                        result['tx_power'] = float(tx_power)
                        result['rssi'] = float(rssi)
                        result['sent_time'] = rfc3339.rfc3339(datetime.datetime.now())
                        self.packet_queue.put(result)
                self.sock.setsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, old_filter)

    def noise_filter(self):
        """
        Get multiple RSSI values for a iBaecon and perform noise filtering, and put it to another Queue
        :return:
        """
        while True:
            if self.b_stop:
                time.sleep(.1)
            else:
                if self.packet_queue.qsize() > 0:
                    new_packet = self.packet_queue.get()
                    mac_list = [p['macid'] for p in self.data_list]
                    if new_packet['macid'] in mac_list:
                        # If this iBeacon is already detected:
                        index = mac_list.index(new_packet['macid'])
                        packet = self.data_list[index]
                        packet['rssi'].append(new_packet['rssi'])
                        if len(packet['rssi']) > int(self.get_param_from_xml('ACCUM_CNT')):
                            # If enough values are received, start filtering
                            avg_rssi = filter_values(packet['rssi'])
                            packet['distance'] = rssi_to_distance(avg_rssi, packet['tx_power'])
                            packet['rssi'] = avg_rssi
                            packet['sent_time'] = rfc3339.rfc3339(datetime.datetime.now())
                            packet['timestamp'] = time.time()
                            self.data_queue.put(packet)
                            # Pop this packet from the list
                            self.data_list.pop(index)
                        else:
                            packet['rssi'].append(new_packet['rssi'])
                    else:
                        new_packet['rssi'] = [new_packet['rssi']]
                        self.data_list.append(new_packet)
                else:
                    time.sleep(.1)

    def key_interrupt(self):
        self.b_stop = True

    def get_scanned_data(self):
        if self.data_queue.qsize() > 0:
            return self.data_queue.get()
        else:
            return None

    @staticmethod
    def le_handle_connection_complete(pkt):
        print pkt


def filter_values(values):
    """
    Filter the values
    :param values:
    :return:
    """
    if len(values) == 1:
        return values[0]
    # Remove maximum & minimum values
    values.sort()
    values = values[1:-1]
    if len(values) == 1:
        return values[0]
    # Remove 20% of high/low values
    avg = reduce(lambda x, y: x + y, values)/len(values)
    result = []
    for val in values:
        if val >= avg:
            if (val - avg) / (values[-1] - avg) < .8:
                result.append(val)
        else:
            if (avg - val) / (avg - values[0]) < .8:
                result.append(val)
    if len(result) == 0:
        return avg
    # Calculate average value again after removing 20%
    avg_val = reduce(lambda x, y: x + y, result) / len(result)
    return round(avg_val, 2)


def rssi_to_distance(rssi, tx_power):
    """
    Convert rssi value to a distance value
    ref: http://developer.radiusnetworks.com/2014/12/04/fundamentals-of-beacon-ranging.html

    :param tx_power: Known measured signal strength in rssi at 1 meter away
    :param rssi: Received Signal Strength Indicator
    :return: RSSI value
    """
    # Typical one meter calibration value for a beacon transmitter is -59 dBm.
    if tx_power >= -10:
        tx_power = -59.0

    if rssi >= 0:
        return -1.0

    ratio = rssi / tx_power
    if ratio < 1.0:
        return round(pow(ratio, 10), 2)
    else:
        return round(0.89976 * pow(ratio, 7.7095) + 0.111, 2)


def to_number(pkt):
    my_int = 0
    multiple = 256
    for c in pkt:
        my_int += struct.unpack("B", c)[0] * multiple
        multiple = 1
    return my_int


def to_string(pkt):
    my_str = ""
    for c in pkt:
        my_str += "%02x" % struct.unpack("B", c)[0]
    return my_str


def print_packet(pkt):
    for c in pkt:
        sys.stdout.write("%02x " % struct.unpack("B", c)[0])


def get_packed_bdaddr(bdaddr_string):
    packable_addr = []
    addr = bdaddr_string.split(':')
    addr.reverse()
    for b in addr:
        packable_addr.append(int(b, 16))
    return struct.pack("<BBBBBB", *packable_addr)


def packed_bdaddr_to_string(bdaddr_packed):
    return ':'.join('%02x' % i for i in struct.unpack("<BBBBBB", bdaddr_packed[::-1]))


def read_local_bdaddr():
    hci_sock = bluez.hci_open_dev(0)
    old_filter = hci_sock.getsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, 14)
    flt = bluez.hci_filter_new()
    opcode = bluez.cmd_opcode_pack(bluez.OGF_INFO_PARAM, bluez.OCF_READ_BD_ADDR)
    bluez.hci_filter_set_ptype(flt, bluez.HCI_EVENT_PKT)
    bluez.hci_filter_set_event(flt, bluez.EVT_CMD_COMPLETE)
    bluez.hci_filter_set_opcode(flt, opcode)
    hci_sock.setsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, flt)
    bluez.hci_send_cmd(hci_sock, bluez.OGF_INFO_PARAM, bluez.OCF_READ_BD_ADDR)
    pkt = hci_sock.recv(255)
    status, raw_bdaddr = struct.unpack("xxxxxxB6s", pkt)
    assert status == 0
    t = ["%X" % ord(str(b)) for b in raw_bdaddr]
    t.reverse()
    bdaddr = ":".join(t)
    # restore old filter
    hci_sock.setsockopt(bluez.SOL_HCI, bluez.HCI_FILTER, old_filter)
    return bdaddr


if __name__ == '__main__':
    if os.geteuid() != 0:
        exit("You need to have root privileges to run this script.\nPlease try again by using 'sudo'.\nExiting...")

    s = BeaconScanner()

    print 'MAC: ', read_local_bdaddr()

    s.start()

    while True:
        d = s.get_scanned_data()
        if d is not None:
            print d
        else:
            time.sleep(.1)
