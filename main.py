import os
import socket
import threading

import requests
import rfc3339
import time
import signal
import sys
import datetime
from pubnub import Pubnub
from ble_scan import BeaconScanner
from base import Base
import logging.config

threadLock = threading.Lock()

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

logging.config.fileConfig(cur_dir + "logging.conf")
logger = logging.getLogger("ibeacon")

b_pubnub = False


class Main(Base):

    inner_distance = 3.0
    outer_distance = 10.0
    scanner = None
    pubnub = None

    outers = []
    inners = []

    b_stop = False
    b_exit = False

    inner_time = 0

    life_time = 10      # Life time of each ibeacon in outer/inner zone

    def __init__(self):
        Base.__init__(self)
        self.scanner = BeaconScanner()
        self.inner_distance = float(self.get_param_from_xml('INNER_ZONE'))
        self.outer_distance = float(self.get_param_from_xml('OUTER_ZONE'))
        self.pubnub = Pubnub(publish_key=self.get_param_from_xml('PUB_KEY'),
                             subscribe_key=self.get_param_from_xml('SUB_KEY'))
        self.inner_time = float(self.get_param_from_xml('INNER_TIME'))

    def run(self):
        signal.signal(signal.SIGINT, self.signal_handler)
        print 'Press CTRL+C to exit'
        # signal.pause()
        if not self.scanner.start():
            logger.error('Failed to start scanner')
            return False
        self.b_stop = False
        threading.Thread(target=self.process_scan_data).start()
        threading.Thread(target=self.check_lifetime).start()
        if self.debug:
            threading.Thread(target=self.print_beacons).start()

        return True

    def process_scan_data(self):
        while True:
            self.inner_distance = float(self.get_param_from_xml('INNER_ZONE'))
            self.outer_distance = float(self.get_param_from_xml('OUTER_ZONE'))

            if self.b_stop:
                time.sleep(.1)
            elif self.b_exit:
                break
            else:
                data = self.scanner.get_scanned_data()
                if data is not None:
                    data['published'] = False

                    # lock acquire
                    threadLock.acquire()

                    out_mac_list = [out['macid'] for out in self.outers]
                    in_mac_list = [inn['macid'] for inn in self.inners]
                    mac = data['macid']
                    if data['distance'] > self.outer_distance:
                        if mac in out_mac_list:
                            self.outers.pop(out_mac_list.index(mac))
                            self.publish_message(zone='OUTER', action='OUT', ibeacon=data)
                        elif mac in in_mac_list:
                            self.inners.pop(in_mac_list.index(mac))
                            self.publish_message(zone='OUTER/INNER', action='OUT', ibeacon=data)
                            # self.publish_message('A student leaved outer zone from inner zone directly.', data)
                        else:
                            logger.debug('- Far student is detected: {}'.format(data))

                    elif data['distance'] >= self.inner_distance:
                        if mac in in_mac_list:   # If this ibeacon was in the inners zone, move this to outers list.
                            logger.debug('A student moved from inner zone to outer zone, mac: {}'.format(mac))
                            data['published'] = self.inners[in_mac_list.index(mac)]['published']
                            self.inners.pop(in_mac_list.index(mac))

                        if mac not in in_mac_list + out_mac_list:
                            self.publish_message(zone='OUTER', action='IN', ibeacon=data)

                        if mac not in out_mac_list:  # iBeacon just entered into outer zone
                            self.outers.append(data)
                        else:      # Update distance
                            self.outers[out_mac_list.index(mac)]['distance'] = data['distance']
                    else:
                        if mac in out_mac_list:
                            logger.debug('A student moved from outer zone to inner zone, mac: {}'.format(mac))
                            self.outers.pop(out_mac_list.index(mac))

                        if mac in in_mac_list:
                            # Calculate this ibeacon's time in the inner zone.
                            if data['timestamp'] - self.inners[in_mac_list.index(mac)]['timestamp'] > self.inner_time:
                                if not self.inners[in_mac_list.index(mac)]['published']:
                                    self.publish_message(action='AUTH', zone='INNER', ibeacon=data)
                                    self.inners[in_mac_list.index(mac)]['published'] = True
                        else:
                            logger.debug('A student entered to inner zone directly..., mac: {}'.format(mac))
                            self.inners.append(data)

                    # free lock to release
                    threadLock.release()

                else:
                    time.sleep(.1)

    def check_lifetime(self):
        """
        Check life time of all ibeacons of inner & outer zone and remove dead ones.
        :return:
        """
        while True:
            if self.b_exit:
                break
            threadLock.acquire()

            for inn in self.inners:
                elapsed = time.time() - inn['timestamp']
                if elapsed > self.life_time and not inn['published']:
                    logger.debug(
                        "A student entered to inner zone, but he is never detected any more, removing: {}".format(
                            inn['macid']))
                    self.inners.remove(inn)

            for out in self.outers:
                elapsed = time.time() - out['timestamp']
                if elapsed > self.life_time:
                    # TODO: Add logic when a student does not enter the inner zone from outer zone for a while.
                    # print "A student entered to outer zone, but he is never entered into inner zone:", out['macid']
                    # self.outers.remove(out)
                    pass

            threadLock.release()
            time.sleep(.5)

    def publish_message(self, zone='', action='IN', ibeacon=None):
        """
        Publish pubnub message on the certain channel
        Type: TIME (with offset+timezone) | UUID (beacon) | MAC (beacon) | HUB (ID?) | ZONE | IN or OUT

        :param action: IN or OUT
        :param zone: INNER or OUTER
        :param ibeacon: ibeacon dictionary
        :return:
        """

        global b_pubnub

        if b_pubnub:
            channel = self.get_param_from_xml('CHANNEL')
            time_now = rfc3339.rfc3339(datetime.datetime.now())
            buf = '{} | {} | {} | {} | {} | {}'.format(time_now, ibeacon['uuid'], ibeacon['macid'], get_serial(), zone,
                                                       action)
            logger.debug('-- Publishing a message : {}'.format(buf))
            self.pubnub.publish(channel=channel, message=buf)
        else:
            url = 'http://handcert.herokuapp.com/api/v1/rpi_events'
            ibeacon['zone'] = zone
            ibeacon['zone_action'] = action
            ibeacon['scanner_name'] = self.get_param_from_xml('CHANNEL')
            ibeacon['scanner_mac'] = self.scanner.mac
            r = requests.post(url, json=ibeacon)
            logger.debug('POST response: '.format(r.content))

    def print_beacons(self):
        """
        Print ibeacons in the inner & outer zone.
        :return:
        """
        while True:
            if self.b_exit:
                break
            logger.debug("----- Inner List -----")
            for inn in self.inners:
                logger.debug(inn)
            logger.debug("----- Outer List -----")
            for out in self.outers:
                logger.debug(out)
            time.sleep(2)

    def signal_handler(self, signal, frame):
        self.b_exit = True
        time.sleep(1)
        sys.exit(0)


def get_serial():
    """
    Get serial number of RPi
    :return:
    """
    sn = "0000000000000000"
    try:
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                sn = line[10:26]
        f.close()
    except:
        sn = "ERROR000000000"
    return sn


if __name__ == '__main__':
    if os.geteuid() != 0:
        exit("You need to have root privileges to run this script.\nPlease try again by using 'sudo'.\nExiting...")

    logger.debug('========== Starting Handcert-RPi ==========')

    try:
        a = Main()
        a.run()
    except KeyboardInterrupt:
        print 'Interrupted'
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

